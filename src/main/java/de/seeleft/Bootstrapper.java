/*
 * This file is licensed under the MIT License and is part of the "discord-bot" project.
 * Copyright (c) 2019 Daniel Riegler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.seeleft;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.LinkedHashMap;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class Bootstrapper
{

    static
    {
        Thread.currentThread().setName("main-thread");
    }

    static class ParameterMap extends LinkedHashMap<String, String>
    {

        private ParameterMap(final @NonNull String[] args)
        {
            super();
            log.debug("===============================================");
            log.debug("new ParameterMap(String[{}])", args.length);
            for (int index = 0; index < args.length; index++)
            {
                var arg = args[index];
                log.debug("[{}] Parsing raw parameter: \"{}\"", index, arg);
                switch (StringUtils.countMatches(arg, "="))
                {
                    case 0:
                        arg = arg.replace("=", "");
                        log.debug("[{}] No value defined; using \"true\" for parameter \"{}\".", index, arg);
                        super.put(arg, "true");
                        break;
                    case 1:
                        final var parts = arg.split("=", 2);
                        if (parts[1].isEmpty())
                        {
                            log.debug("[{}] Skipping parameter \"{}\" for containing empty value.", index, parts[0]);
                            break;
                        }
                        log.debug("[{}] Using \"{}\" for parameter \"{}\".", index, parts[1], parts[0]);
                        super.put(parts[0], parts[1]);
                        break;
                    default:
                        log.debug("[{}] Skipping parameter \"{}\" for containing more than one '='.", index, arg);
                        break;
                }
            }
            log.debug("===============================================");
        }

        @SuppressWarnings("SameParameterValue")
        boolean getBoolean(final @NonNull Object object)
        {
            return Boolean.parseBoolean(super.getOrDefault(object, "false"));
        }

    }


    public static void main(final String[] args)
    {

        log.info("Bootstrapping...");

        // parse parameter map and bootstrap
        final var instance = new SeeleftDiscord(new ParameterMap(Arrays.stream(args).
                map(arg -> arg = arg.strip()).
                filter(arg -> arg.startsWith("--")).
                map(arg -> arg.replaceFirst("--", "")).
                toArray(String[]::new)
        ));

        // add shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(instance::shutdown, "shutdown-thread"));

    }

}
