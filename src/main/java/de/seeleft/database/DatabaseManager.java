/*
 * This file is licensed under the MIT License and is part of the "discord-bot" project.
 * Copyright (c) 2019 Daniel Riegler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.seeleft.database;

import com.cyr1en.flatdb.Database;
import com.cyr1en.flatdb.DatabaseBuilder;
import com.cyr1en.flatdb.annotations.Table;
import lombok.NonNull;
import org.reflections.Reflections;

import java.io.Closeable;
import java.io.IOException;
import java.sql.SQLException;
public class DatabaseManager implements Closeable
{

    private final Database database;

    public DatabaseManager(final @NonNull String databasePath)
    {
        final var builder = new DatabaseBuilder().
                setDatabasePrefix("db_").
                setPath(databasePath);

        // append tables
        new Reflections("de.seeleft.database.tables").getSubTypesOf(ITable.class).forEach(tableClass -> {
            if (tableClass.isAnnotationPresent(Table.class)){
                System.out.println("TABLE: " + tableClass.getName());
                builder.appendTable(tableClass);
            }
        });

        try
        {
            this.database = builder.build();
        }
        catch (final SQLException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws IOException
    {
        try
        {
            if (!this.database.getConnection().isClosed())
                this.database.getConnection().close();
        }
        catch (final SQLException e)
        {
            throw new IOException(e);
        }
    }
}
