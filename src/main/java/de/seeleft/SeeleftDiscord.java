/*
 * This file is licensed under the MIT License and is part of the "discord-bot" project.
 * Copyright (c) 2019 Daniel Riegler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package de.seeleft;

import com.google.gson.Gson;
import com.moandjiezana.toml.Toml;
import de.seeleft.database.DatabaseManager;
import de.seeleft.database.tables.SettingsTable;
import de.seeleft.event.IEventSubsriber;
import discord4j.core.DiscordClient;
import discord4j.core.DiscordClientBuilder;
import discord4j.core.event.domain.Event;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.extern.log4j.Log4j2;
import org.reflections.Reflections;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.LinkedList;
import java.util.Objects;

@Log4j2
@Accessors(fluent = true)
@SuppressWarnings("unchecked")
class SeeleftDiscord
{

    private final Toml config = new Toml();

    private final DatabaseManager databaseManager;

    private final DiscordClient discordClient;

    private final Bootstrapper.ParameterMap parameterMap;

    SeeleftDiscord(final @NonNull Bootstrapper.ParameterMap map)
    {
        synchronized (this)
        {

            this.parameterMap = map;

            if (map.getBoolean("debug"))
                log.warn("Running in debug-mode. Please remove the \"--debug\" parameter if using in production.");

            final var configFile = new File(map.getOrDefault("config", "./config.toml"));
            log.debug("Using config in {}.", configFile.getPath());

            // copy default config from classpath
            if (!configFile.exists())
            {
                log.debug("Copying default config from classpath.");
                try (final var stream = ClassLoader.getSystemResourceAsStream("config.default.toml"))
                {
                    Files.copy(Objects.requireNonNull(stream), configFile.toPath());
                    log.debug("Successfully copied default config from classpath.");
                }
                catch (final IOException e)
                {
                    log.fatal("Could not copy default config from classpath.", e);
                }
            }

            // parse config
            log.debug("Parsing config...");
            this.config.read(configFile);
            log.debug("Successfully parsed config.");

            // init database
            this.databaseManager = new DatabaseManager(this.getOption("settings.database.path", "./database.sqlite"));

            // create discord client
            this.discordClient = new DiscordClientBuilder(this.getOption("services.discord.token", null)).build();

            log.debug("Registering events...");
            final var eventSubsriber = new LinkedList<IEventSubsriber>();

            // register events
            new Reflections("de.seeleft.event.listener").getSubTypesOf(IEventSubsriber.class).forEach(eventClass -> {
                log.debug("Trying to instantiate eventsubsriber at {}...", eventClass.getCanonicalName());
                try
                {
                    log.debug("Initialized and added eventsubscriber at {}.", eventClass.getCanonicalName());
                    eventSubsriber.add(eventClass.getDeclaredConstructor().newInstance());
                }
                catch (final InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e)
                {
                    log.warn("Could not instantiate eventsubscriber at {}.", eventClass.getCanonicalName(), e);
                }
            });

            // subscribe to events
            this.discordClient.getEventDispatcher().on(Event.class).subscribe(event -> eventSubsriber.stream().
                    filter(subscriber -> subscriber.getClass().isAssignableFrom(event.getClass())).
                    forEach(subscriber -> subscriber.call(event))
            );

            // todo login to discord

        }
    }

    public String getOption(final @NonNull String key, final String def, final Object... args)
    {
        var string = this.parameterMap.getOrDefault(key.replace('.', '-'),
                this.config.getString(key.replace('-', '.'), def));
        return null != args ? MessageFormat.format(string, args) : string;
    }

    synchronized void shutdown()
    {
        // logout from discord
        if (this.discordClient.isConnected())
            this.discordClient.logout().block();

        // close database
        try
        {
            this.databaseManager.close();
        }
        catch (final IOException e)
        {
            log.warn("Could not close database.", e);
        }
    }

}
